import vrep
import time
import numpy as np
from numpy.linalg import multi_dot
from scipy.linalg import expm, logm
import math

def createSkew(mat):
    skew = np.array([
    [0,-mat[2][0],mat[1][0]],
    [mat[2][0],0,-mat[0][0]],
    [-mat[1][0],mat[0][0],0]
    ])

    return skew

def createScrew(a):
    screw = np.array([
    [0],
    [0],
    [0],
    [a[0][0]],
    [a[1][0]],
    [a[2][0]]
    ])
    return screw

def createScrewQ(a, q):

    aq = -np.dot(createSkew(a), q)

    screw = np.array([
    [a[0][0]],
    [a[1][0]],
    [a[2][0]],
    [aq[0][0]],
    [aq[1][0]],
    [aq[2][0]]
    ])

    return screw

def screwBracForm(mat) :

     brac = np.array([
     [0, -1 * mat[2][0], mat[1][0], mat[3][0]],
     [mat[2][0], 0, -1 * mat[0][0], mat[4][0]],
     [-1 * mat[1][0], mat[0][0], 0, mat[5][0]],
     [0,0,0,0]
     ])
     return brac

def createAdjoint(mat):

    R = np.array([
    [mat[0][0],mat[0][1],mat[0][2]],
    [mat[1][0],mat[1][1],mat[1][2]],
    [mat[2][0],mat[2][1],mat[2][2]]
    ])
    p = np.array([
    [mat[0][3]],
    [mat[1][3]],
    [mat[2][3]]
    ])

    bottomLeft = np.dot(createSkew(p), R)

    adj = np.array([
    [R[0][0],R[0][1],R[0][2],0,0,0],
    [R[1][0],R[1][1],R[1][2],0,0,0],
    [R[2][0],R[2][1],R[2][2],0,0,0],
    [bottomLeft[0][0],bottomLeft[0][1],bottomLeft[0][2], R[0][0],R[0][1],R[0][2]],
    [bottomLeft[1][0],bottomLeft[1][1],bottomLeft[1][2], R[1][0],R[1][1],R[1][2]],
    [bottomLeft[2][0],bottomLeft[2][1],bottomLeft[2][2], R[2][0],R[2][1],R[2][2]]
    ])
    return adj

def invScrewBrac(mat):

    twist = np.array([
    [mat[2][1]],
    [mat[0][2]],
    [mat[1][0]],
    [mat[0][3]],
    [mat[1][3]],
    [mat[2][3]]
    ])

    return twist

def invSkewBrac(mat):

    inv_skew = np.array([
    [mat[2][1]],
    [mat[0][2]],
    [mat[1][0]]
    ])

    return inv_skew

def getSpacialJacobian(S, theta):

    mat1 = np.array([
    [S[0][0]],
    [S[1][0]],
    [S[2][0]],
    [S[3][0]],
    [S[4][0]],
    [S[5][0]]
    ])
    brac1 = screwBracForm(mat1) * theta[0]
    mat1adj = createAdjoint(expm(brac1))

    mat2 = np.array([
    [S[0][1]],
    [S[1][1]],
    [S[2][1]],
    [S[3][1]],
    [S[4][1]],
    [S[5][1]]
    ])
    brac2 = screwBracForm(mat2) * theta[1]
    mat2adj = createAdjoint(expm(brac2))
    mat2 = np.dot(mat1adj, mat2)

    mat3 = np.array([
    [S[0][2]],
    [S[1][2]],
    [S[2][2]],
    [S[3][2]],
    [S[4][2]],
    [S[5][2]]
    ])
    brac3 = screwBracForm(mat3) * theta[2]
    mat3adj = createAdjoint(expm(brac3))
    mat3 = np.linalg.multi_dot([mat1adj, mat2adj, mat3])

    mat4 = np.array([
    [S[0][3]],
    [S[1][3]],
    [S[2][3]],
    [S[3][3]],
    [S[4][3]],
    [S[5][3]]
    ])
    brac4 = screwBracForm(mat4) * theta[3]
    mat4adj = createAdjoint(expm(brac4))
    mat4 = np.linalg.multi_dot([mat1adj, mat2adj, mat3adj, mat4])


    mat5 = np.array([
    [S[0][4]],
    [S[1][4]],
    [S[2][4]],
    [S[3][4]],
    [S[4][4]],
    [S[5][4]]
    ])
    brac5 = screwBracForm(mat5) * theta[4]
    mat5adj = createAdjoint(expm(brac5))
    mat5 = np.linalg.multi_dot([mat1adj, mat2adj, mat3adj, mat4adj, mat5])

    mat6 = np.array([
    [S[0][5]],
    [S[1][5]],
    [S[2][5]],
    [S[3][5]],
    [S[4][5]],
    [S[5][5]]
    ])
    brac6 = screwBracForm(mat6) * theta[5]
    mat6adj = createAdjoint(expm(brac6))
    mat6 = np.linalg.multi_dot([mat1adj, mat2adj, mat3adj, mat4adj, mat5adj, mat6])


    j = np.array([
    [mat1[0][0],mat2[0][0], mat3[0][0], mat4[0][0], mat5[0][0], mat6[0][0]],
    [mat1[1][0],mat2[1][0], mat3[1][0], mat4[1][0], mat5[1][0], mat6[1][0]],
    [mat1[2][0],mat2[2][0], mat3[2][0], mat4[2][0], mat5[2][0], mat6[2][0]],
    [mat1[3][0],mat2[3][0], mat3[3][0], mat4[3][0], mat5[3][0], mat6[3][0]],
    [mat1[4][0],mat2[4][0], mat3[4][0], mat4[4][0], mat5[4][0], mat6[4][0]],
    [mat1[5][0],mat2[5][0], mat3[5][0], mat4[5][0], mat5[5][0], mat6[5][0]]
    ])

    return j

def getT_1in0(M, S, theta):

    mat1 = np.array([
    [S[0][0]],
    [S[1][0]],
    [S[2][0]],
    [S[3][0]],
    [S[4][0]],
    [S[5][0]]
    ])
    brac1 = screwBracForm(mat1) * theta[0]

    mat2 = np.array([
    [S[0][1]],
    [S[1][1]],
    [S[2][1]],
    [S[3][1]],
    [S[4][1]],
    [S[5][1]]
    ])
    brac2 = screwBracForm(mat2) * theta[1]

    mat3 = np.array([
    [S[0][2]],
    [S[1][2]],
    [S[2][2]],
    [S[3][2]],
    [S[4][2]],
    [S[5][2]]
    ])
    brac3 = screwBracForm(mat3) * theta[2]

    mat4 = np.array([
    [S[0][3]],
    [S[1][3]],
    [S[2][3]],
    [S[3][3]],
    [S[4][3]],
    [S[5][3]]
    ])
    brac4 = screwBracForm(mat4) * theta[3]

    mat5 = np.array([
    [S[0][4]],
    [S[1][4]],
    [S[2][4]],
    [S[3][4]],
    [S[4][4]],
    [S[5][4]]
    ])
    brac5 = screwBracForm(mat5) * theta[4]

    mat6 = np.array([
    [S[0][5]],
    [S[1][5]],
    [S[2][5]],
    [S[3][5]],
    [S[4][5]],
    [S[5][5]]
    ])
    brac6 = screwBracForm(mat6) * theta[5]

    T = multi_dot([expm(brac1), expm(brac2), expm(brac3), expm(brac4), expm(brac5), expm(brac6), M])

    return T

################################################################################

# Close all open connections (just in case)
vrep.simxFinish(-1)

# Connect to V-REP (raise exception on failure)
clientID = vrep.simxStart('127.0.0.1', 19997, True, True, 5000, 5)
if clientID == -1:
    raise Exception('Failed connecting to remote API server')


################### Get handles for all joints #################################

result, joint_one_handle = vrep.simxGetObjectHandle(clientID, 'UR3_joint1', vrep.simx_opmode_blocking)
if result != vrep.simx_return_ok:
    raise Exception('could not get object handle for first joint')

result, joint_one_2_handle = vrep.simxGetObjectHandle(clientID, 'UR3_joint1_2', vrep.simx_opmode_blocking)
if result != vrep.simx_return_ok:
    raise Exception('could not get object handle for first joint1_2')

result, joint_two_handle = vrep.simxGetObjectHandle(clientID, 'UR3_joint2', vrep.simx_opmode_blocking)
if result != vrep.simx_return_ok:
    raise Exception('could not get object handle for second joint')

result, joint_two_2_handle = vrep.simxGetObjectHandle(clientID, 'UR3_joint2_2', vrep.simx_opmode_blocking)
if result != vrep.simx_return_ok:
    raise Exception('could not get object handle for first joint2_2')

result, joint_three_handle = vrep.simxGetObjectHandle(clientID, 'UR3_joint3', vrep.simx_opmode_blocking)
if result != vrep.simx_return_ok:
    raise Exception('could not get object handle for third joint')

result, joint_three_2_handle = vrep.simxGetObjectHandle(clientID, 'UR3_joint3_2', vrep.simx_opmode_blocking)
if result != vrep.simx_return_ok:
    raise Exception('could not get object handle for first joint3_2')

result, joint_four_handle = vrep.simxGetObjectHandle(clientID, 'UR3_joint4', vrep.simx_opmode_blocking)
if result != vrep.simx_return_ok:
    raise Exception('could not get object handle for fourth joint')

result, joint_four_2_handle = vrep.simxGetObjectHandle(clientID, 'UR3_joint4_2', vrep.simx_opmode_blocking)
if result != vrep.simx_return_ok:
    raise Exception('could not get object handle for fourth joint4_2')

result, joint_five_handle = vrep.simxGetObjectHandle(clientID, 'UR3_joint5', vrep.simx_opmode_blocking)
if result != vrep.simx_return_ok:
    raise Exception('could not get object handle for fifth joint')

result, joint_five_2_handle = vrep.simxGetObjectHandle(clientID, 'UR3_joint5_2', vrep.simx_opmode_blocking)
if result != vrep.simx_return_ok:
    raise Exception('could not get object handle for first joint3_2')

result, joint_six_handle = vrep.simxGetObjectHandle(clientID, 'UR3_joint6', vrep.simx_opmode_blocking)
if result != vrep.simx_return_ok:
    raise Exception('could not get object handle for sixth joint')

result, joint_six_2_handle = vrep.simxGetObjectHandle(clientID, 'UR3_joint6_2', vrep.simx_opmode_blocking)
if result != vrep.simx_return_ok:
    raise Exception('could not get object handle for sixth joint6_2')
################################################################################

# Start simulation
vrep.simxStartSimulation(clientID, vrep.simx_opmode_oneshot)

# Wait two seconds
time.sleep(2)

############### Get the values for joint variables #############################

result1, theta1 = vrep.simxGetJointPosition(clientID, joint_one_handle, vrep.simx_opmode_blocking)
if result1 != vrep.simx_return_ok:
    raise Exception('could not get first joint variable')
print('current value of first joint variable on UR3: theta = {:f}'.format(theta1))

result1_2, theta1_2 = vrep.simxGetJointPosition(clientID, joint_one_2_handle, vrep.simx_opmode_blocking)
if result1_2 != vrep.simx_return_ok:
    raise Exception('could not get first_2 joint variable')
print('current value of first joint variable on UR3_2: theta = {:f}'.format(theta1_2))

result2, theta2 = vrep.simxGetJointPosition(clientID, joint_two_handle, vrep.simx_opmode_blocking)
if result2 != vrep.simx_return_ok:
    raise Exception('could not get second joint variable')
print('current value of second joint variable on UR3: theta = {:f}'.format(theta2))

result2_2, theta2_2 = vrep.simxGetJointPosition(clientID, joint_two_2_handle, vrep.simx_opmode_blocking)
if result2_2 != vrep.simx_return_ok:
    raise Exception('could not get second_2 joint variable')
print('current value of second joint variable on UR3_2: theta = {:f}'.format(theta2_2))

result3, theta3 = vrep.simxGetJointPosition(clientID, joint_three_handle, vrep.simx_opmode_blocking)
if result3 != vrep.simx_return_ok:
    raise Exception('could not get third joint variable')
print('current value of third joint variable on UR3: theta = {:f}'.format(theta3))

result3_2, theta3_2 = vrep.simxGetJointPosition(clientID, joint_three_2_handle, vrep.simx_opmode_blocking)
if result3_2 != vrep.simx_return_ok:
    raise Exception('could not get third_2 joint variable')
print('current value of third joint variable on UR3_2: theta = {:f}'.format(theta3_2))

result4, theta4 = vrep.simxGetJointPosition(clientID, joint_four_handle, vrep.simx_opmode_blocking)
if result4 != vrep.simx_return_ok:
    raise Exception('could not get fourth joint variable')
print('current value of fourth joint variable on UR3: theta = {:f}'.format(theta4))

result4_2, theta4_2 = vrep.simxGetJointPosition(clientID, joint_four_2_handle, vrep.simx_opmode_blocking)
if result4_2 != vrep.simx_return_ok:
    raise Exception('could not get fourth_2 joint variable')
print('current value of fourth joint variable on UR3_2: theta = {:f}'.format(theta4_2))

result5, theta5 = vrep.simxGetJointPosition(clientID, joint_five_handle, vrep.simx_opmode_blocking)
if result5 != vrep.simx_return_ok:
    raise Exception('could not get fifth joint variable')
print('current value of fifth joint variable on UR3: theta = {:f}'.format(theta5))

result5_2, theta5_2 = vrep.simxGetJointPosition(clientID, joint_five_2_handle, vrep.simx_opmode_blocking)
if result5_2 != vrep.simx_return_ok:
    raise Exception('could not get fifth_2 joint variable')
print('current value of fifth joint variable on UR3_2: theta = {:f}'.format(theta5_2))

result6, theta6 = vrep.simxGetJointPosition(clientID, joint_six_handle, vrep.simx_opmode_blocking)
if result6 != vrep.simx_return_ok:
    raise Exception('could not get sixth joint variable')
print('current value of sixth joint variable on UR3: theta = {:f}'.format(theta6))

result6_2, theta6_2 = vrep.simxGetJointPosition(clientID, joint_six_2_handle, vrep.simx_opmode_blocking)
if result6_2 != vrep.simx_return_ok:
    raise Exception('could not get sixth_2 joint variable')
print('current value of sixth joint variable on UR3_2: theta = {:f}'.format(theta6_2))
################################################################################
# Moving the robots:

#Angles for the movement of the robot joints
movementAngle1 = (-np.pi / 6)
movementAngle1_2 = (-np.pi / 6)
movementAngle2 = (np.pi / 6)
movementAngle2_2 = (np.pi / 6)
movementAngle3 = (-np.pi / 4)
movementAngle3_2 = (-np.pi / 4)
movementAngle4 = (np.pi / 4)
movementAngle4_2 = (-np.pi / 4)
movementAngle5 = (np.pi / 3)
movementAngle5_2 = (-np.pi / 3)
movementAngle6 = (np.pi / 2)
movementAngle6_2 = (-np.pi / 2.5)

 #Calculating the the Screw Matracies of the robot1
a = np.array([
[0],
[0],
[1]
])
q = np.array([
[0],
[0],
[0.1045]
])

mat1 = createScrewQ(a, q)
brac1 = screwBracForm(mat1)  * movementAngle1

a = np.array([
[-1],
[0],
[0]
])
q = np.array([
[-0.117],
[0],
[0.1089]
])

mat2 = createScrewQ(a, q)
brac2 = screwBracForm(mat2) * movementAngle2

a = np.array([
[1],
[0],
[0]
])
q = np.array([
[-0.117],
[0],
[0.3525]
])

mat3 = createScrewQ(a, q)
brac3 = screwBracForm(mat3) * movementAngle3

a = np.array([
[-1],
[0],
[0]
])
q = np.array([
[-0.117],
[0],
[0.5658]
])

mat4 = createScrewQ(a, q)
brac4 = screwBracForm(mat4) * movementAngle4

a = np.array([
[0],
[0],
[1]
])
q = np.array([
[-0.1123],
[0],
[0.65]
])

mat5 = createScrewQ(a, q)
brac5 = screwBracForm(mat5) * movementAngle5

a = np.array([
[-1],
[0],
[0]
])
q = np.array([
[-0.117],
[0],
[0.6511]
])

mat6 = createScrewQ(a, q)
brac6 = screwBracForm(mat6) * movementAngle6

#Initial Pose of the end effector
M = np.array([
[0.0, 0.0, -1.0, -0.2503],
[0.0, 1.0, 0.0, 0.0],
[1.0, 0.0, 0.0, 0.6513],
[0, 0, 0, 1]
])




# print("Initial Pose")
# print(M)

#Pose of the end effect based on the anlge values
T = multi_dot([expm(brac1), expm(brac2), expm(brac3), expm(brac4), expm(brac5), expm(brac6), M])

# print("Pose After Movement")
# print(T)

R = np.array([
[T[0][0],T[0][1],T[0][2]],
[T[1][0],T[1][1],T[1][2]],
[T[2][0],T[2][1],T[2][2]]
])

w_brac = logm(R)

w = invSkewBrac(w_brac)
# print(w)
theta = np.linalg.norm(w)
a = w / theta

q0 = math.cos(theta/2)
brac = a * math.sin(theta/2)
q1 = brac[0][0]
q2 = brac[1][0]
q3 = brac[2][0]

#Set the desired value of the joint variables
vrep.simxSetJointTargetPosition(clientID, joint_one_handle, theta1 + movementAngle1, vrep.simx_opmode_oneshot)
vrep.simxSetJointTargetPosition(clientID, joint_one_2_handle, theta1_2 + movementAngle1_2, vrep.simx_opmode_oneshot)
time.sleep(1)
vrep.simxSetJointTargetPosition(clientID, joint_two_handle, theta2 + movementAngle2, vrep.simx_opmode_oneshot)
vrep.simxSetJointTargetPosition(clientID, joint_two_2_handle, theta2_2 + movementAngle2_2, vrep.simx_opmode_oneshot)
time.sleep(1)
vrep.simxSetJointTargetPosition(clientID, joint_three_handle, theta3 + movementAngle3, vrep.simx_opmode_oneshot)
vrep.simxSetJointTargetPosition(clientID, joint_three_2_handle, theta3_2 + movementAngle3_2, vrep.simx_opmode_oneshot)
time.sleep(1)
vrep.simxSetJointTargetPosition(clientID, joint_four_handle, theta4 + movementAngle4, vrep.simx_opmode_oneshot)
vrep.simxSetJointTargetPosition(clientID, joint_four_2_handle, theta4_2 + movementAngle4_2, vrep.simx_opmode_oneshot)
time.sleep(1)
vrep.simxSetJointTargetPosition(clientID, joint_five_handle, theta5 + movementAngle5, vrep.simx_opmode_oneshot)
vrep.simxSetJointTargetPosition(clientID, joint_five_2_handle, theta5_2 + movementAngle5_2, vrep.simx_opmode_oneshot)
time.sleep(1)
vrep.simxSetJointTargetPosition(clientID, joint_six_handle, theta6 + movementAngle6, vrep.simx_opmode_oneshot)
vrep.simxSetJointTargetPosition(clientID, joint_six_2_handle, theta6_2 + movementAngle6_2, vrep.simx_opmode_oneshot)

# print("\n\n\n\n\n\n\n\n\n\n\nSay Hi!\n\n\n\n\n\n\n\n\n\n")
# print('Hands should open now')
time.sleep(2)
######################################################

#Angles for the movement of the robot joints
movementAngle1 = (-np.pi / 3)
movementAngle1_2 = (-np.pi / 6)
movementAngle2 = (np.pi / 4)
movementAngle2_2 = (np.pi / 6)
movementAngle3 = (-np.pi / 8)
movementAngle3_2 = (-np.pi / 4)
movementAngle4 = (np.pi / 4)
movementAngle4_2 = (-np.pi / 4)
movementAngle5 = (np.pi / 3)
movementAngle5_2 = (-np.pi / 3)
movementAngle6 = (np.pi / 2)
movementAngle6_2 = (-np.pi / 2.5)


#Initial Pose of the end effector
M = np.array([
[0.0, 0.0, -1.0, -0.2503],
[0.0, 1.0, 0.0, 0.0],
[1.0, 0.0, 0.0, 0.6513],
[0, 0, 0, 1]
])




# print("Initial Pose")
# print(M)

#Pose of the end effect based on the anlge values
T = multi_dot([expm(brac1), expm(brac2), expm(brac3), expm(brac4), expm(brac5), expm(brac6), M])

# print("Pose After Movement")
# print(T)

R = np.array([
[T[0][0],T[0][1],T[0][2]],
[T[1][0],T[1][1],T[1][2]],
[T[2][0],T[2][1],T[2][2]]
])

w_brac = logm(R)

w = invSkewBrac(w_brac)
# print(w)
theta = np.linalg.norm(w)
a = w / theta

q0 = math.cos(theta/2)
brac = a * math.sin(theta/2)
q1 = brac[0][0]
q2 = brac[1][0]
q3 = brac[2][0]

#Set the desired value of the joint variables
vrep.simxSetJointTargetPosition(clientID, joint_one_handle, theta1 + movementAngle1, vrep.simx_opmode_oneshot)
vrep.simxSetJointTargetPosition(clientID, joint_one_2_handle, theta1_2 + movementAngle1_2, vrep.simx_opmode_oneshot)
time.sleep(1)
vrep.simxSetJointTargetPosition(clientID, joint_two_handle, theta2 + movementAngle2, vrep.simx_opmode_oneshot)
vrep.simxSetJointTargetPosition(clientID, joint_two_2_handle, theta2_2 + movementAngle2_2, vrep.simx_opmode_oneshot)
time.sleep(1)
vrep.simxSetJointTargetPosition(clientID, joint_three_handle, theta3 + movementAngle3, vrep.simx_opmode_oneshot)
vrep.simxSetJointTargetPosition(clientID, joint_three_2_handle, theta3_2 + movementAngle3_2, vrep.simx_opmode_oneshot)
time.sleep(1)
vrep.simxSetJointTargetPosition(clientID, joint_four_handle, theta4 + movementAngle4, vrep.simx_opmode_oneshot)
vrep.simxSetJointTargetPosition(clientID, joint_four_2_handle, theta4_2 + movementAngle4_2, vrep.simx_opmode_oneshot)
time.sleep(1)
vrep.simxSetJointTargetPosition(clientID, joint_five_handle, theta5 + movementAngle5, vrep.simx_opmode_oneshot)
vrep.simxSetJointTargetPosition(clientID, joint_five_2_handle, theta5_2 + movementAngle5_2, vrep.simx_opmode_oneshot)
time.sleep(1)
vrep.simxSetJointTargetPosition(clientID, joint_six_handle, theta6 + movementAngle6, vrep.simx_opmode_oneshot)
vrep.simxSetJointTargetPosition(clientID, joint_six_2_handle, theta6_2 + movementAngle6_2, vrep.simx_opmode_oneshot)

# print('Hands should open now')
time.sleep(2)

####################################################################

#Angles for the movement of the robot joints
movementAngle1 = (-np.pi / 3)
movementAngle1_2 = (-np.pi / 6)
movementAngle2 = (np.pi / 4)
movementAngle2_2 = (np.pi / 6)
movementAngle3 = (-np.pi / 8)
movementAngle3_2 = (-np.pi / 4)
movementAngle4 = (np.pi / 3)
movementAngle4_2 = (-np.pi / 4)
movementAngle5 = (np.pi / 7)
movementAngle5_2 = (-np.pi / 3)
movementAngle6 = (np.pi / 9)
movementAngle6_2 = (-np.pi / 2.5)

#Initial Pose of the end effector
M = np.array([
[0.0, 0.0, -1.0, -0.2503],
[0.0, 1.0, 0.0, 0.0],
[1.0, 0.0, 0.0, 0.6513],
[0, 0, 0, 1]
])




# print("Initial Pose")
# print(M)

#Pose of the end effect based on the anlge values
T = multi_dot([expm(brac1), expm(brac2), expm(brac3), expm(brac4), expm(brac5), expm(brac6), M])

# print("Pose After Movement")
# print(T)

R = np.array([
[T[0][0],T[0][1],T[0][2]],
[T[1][0],T[1][1],T[1][2]],
[T[2][0],T[2][1],T[2][2]]
])

w_brac = logm(R)

w = invSkewBrac(w_brac)
# print(w)
theta = np.linalg.norm(w)
a = w / theta

q0 = math.cos(theta/2)
brac = a * math.sin(theta/2)
q1 = brac[0][0]
q2 = brac[1][0]
q3 = brac[2][0]

#Set the desired value of the joint variables
vrep.simxSetJointTargetPosition(clientID, joint_one_handle, theta1 + movementAngle1, vrep.simx_opmode_oneshot)
vrep.simxSetJointTargetPosition(clientID, joint_one_2_handle, theta1_2 + movementAngle1_2, vrep.simx_opmode_oneshot)
time.sleep(1)
vrep.simxSetJointTargetPosition(clientID, joint_two_handle, theta2 + movementAngle2, vrep.simx_opmode_oneshot)
vrep.simxSetJointTargetPosition(clientID, joint_two_2_handle, theta2_2 + movementAngle2_2, vrep.simx_opmode_oneshot)
time.sleep(1)
vrep.simxSetJointTargetPosition(clientID, joint_three_handle, theta3 + movementAngle3, vrep.simx_opmode_oneshot)
vrep.simxSetJointTargetPosition(clientID, joint_three_2_handle, theta3_2 + movementAngle3_2, vrep.simx_opmode_oneshot)
time.sleep(1)
vrep.simxSetJointTargetPosition(clientID, joint_four_handle, theta4 + movementAngle4, vrep.simx_opmode_oneshot)
vrep.simxSetJointTargetPosition(clientID, joint_four_2_handle, theta4_2 + movementAngle4_2, vrep.simx_opmode_oneshot)
time.sleep(1)
vrep.simxSetJointTargetPosition(clientID, joint_five_handle, theta5 + movementAngle5, vrep.simx_opmode_oneshot)
vrep.simxSetJointTargetPosition(clientID, joint_five_2_handle, theta5_2 + movementAngle5_2, vrep.simx_opmode_oneshot)
time.sleep(1)
vrep.simxSetJointTargetPosition(clientID, joint_six_handle, theta6 + movementAngle6, vrep.simx_opmode_oneshot)
vrep.simxSetJointTargetPosition(clientID, joint_six_2_handle, theta6_2 + movementAngle6_2, vrep.simx_opmode_oneshot)

time.sleep(1)
# print('Hands should open now')
time.sleep(5)

############### Get the current values for the joints to display ###############
# Get the current value of the first joint variable
result1, theta1 = vrep.simxGetJointPosition(clientID, joint_one_handle, vrep.simx_opmode_blocking)
if result1 != vrep.simx_return_ok:
    raise Exception('could not get first joint variable')
print('current value of first joint variable on UR3: theta = {:f}'.format(theta1))

result1_2, theta1_2 = vrep.simxGetJointPosition(clientID, joint_one_2_handle, vrep.simx_opmode_blocking)
if result1_2 != vrep.simx_return_ok:
    raise Exception('could not get first joint_2 variable')
print('current value of first joint_2 variable on UR3_2: theta = {:f}'.format(theta1_2))

result2, theta2 = vrep.simxGetJointPosition(clientID, joint_two_handle, vrep.simx_opmode_blocking)
if result2 != vrep.simx_return_ok:
    raise Exception('could not get second joint variable')
print('current value of second joint variable on UR3: theta = {:f}'.format(theta2))

result2_2, theta2_2 = vrep.simxGetJointPosition(clientID, joint_two_2_handle, vrep.simx_opmode_blocking)
if result2_2 != vrep.simx_return_ok:
    raise Exception('could not get second joint_2 variable')
print('current value of second joint_2 variable on UR3_2: theta = {:f}'.format(theta2_2))

result3, theta3 = vrep.simxGetJointPosition(clientID, joint_three_handle, vrep.simx_opmode_blocking)
if result3 != vrep.simx_return_ok:
    raise Exception('could not get third joint variable')
print('current value of third joint variable on UR3: theta = {:f}'.format(theta3))

result3_2, theta3_2 = vrep.simxGetJointPosition(clientID, joint_three_2_handle, vrep.simx_opmode_blocking)
if result3_2 != vrep.simx_return_ok:
    raise Exception('could not get third joint_2 variable')
print('current value of third joint_2 variable on UR3_2: theta = {:f}'.format(theta3_2))

result4, theta4 = vrep.simxGetJointPosition(clientID, joint_four_handle, vrep.simx_opmode_blocking)
if result4 != vrep.simx_return_ok:
    raise Exception('could not get fourth joint variable')
print('current value of fourth joint variable on UR3: theta = {:f}'.format(theta4))

result4_2, theta4_2 = vrep.simxGetJointPosition(clientID, joint_four_2_handle, vrep.simx_opmode_blocking)
if result4_2 != vrep.simx_return_ok:
    raise Exception('could not get fourth joint_2 variable')
print('current value of fourth joint_2 variable on UR3_2: theta = {:f}'.format(theta4_2))

result5, theta5 = vrep.simxGetJointPosition(clientID, joint_five_handle, vrep.simx_opmode_blocking)
if result5 != vrep.simx_return_ok:
    raise Exception('could not get fifth joint variable')
print('current value of fifth joint variable on UR3: theta = {:f}'.format(theta5))

result5_2, theta5_2 = vrep.simxGetJointPosition(clientID, joint_five_2_handle, vrep.simx_opmode_blocking)
if result5_2 != vrep.simx_return_ok:
    raise Exception('could not get fifth joint_2 variable')
print('current value of fifth joint_2 variable on UR3_2: theta = {:f}'.format(theta5_2))

result6, theta6 = vrep.simxGetJointPosition(clientID, joint_six_handle, vrep.simx_opmode_blocking)
if result6 != vrep.simx_return_ok:
    raise Exception('could not get sixth joint variable')
print('current value of sixth joint variable on UR3: theta = {:f}'.format(theta6))

result6_2, theta6_2 = vrep.simxGetJointPosition(clientID, joint_six_2_handle, vrep.simx_opmode_blocking)
if result6_2 != vrep.simx_return_ok:
    raise Exception('could not get sixth joint_2 variable')
print('current value of sixth joint_2 variable on UR3_2: theta = {:f}'.format(theta6_2))
################################################################################

# Stop simulation
vrep.simxStopSimulation(clientID, vrep.simx_opmode_oneshot)

# Before closing the connection to V-REP, make sure that the last command sent out had time to arrive. You can guarantee this with (for example):
vrep.simxGetPingTime(clientID)

# Close the connection to V-REP
vrep.simxFinish(clientID)
