## Checkpoint 4 (due 4/2/18)

### Demonstrate Collision Detection for UR3
First, make sure you have Remote API file and related Python files in the same folder as Checkpoint4.py.
You should also load the appropriate scene in the V-REP. The code does not ask for any user input. 
We predefined some of the theta values, which can be looked up in the code. Based on these values,
the collision checker is ran. You can checkout the code in Collision Detection section.

Before the robot moves, the collision checker checks if the collision will occur and will print the 
appropriate comment to the screen. Then it will move the joints. During the first "iteration",
V-REP is not running the simulation, therefore, the joints are allowed to move throught the obstacles.
During the second "iteration", V-REP is in simulation mode, therefore the joints will not be
able to move throught the obstacles no more. For both iterations, same joint configuration was used.


### Collision Detection
The implementation of collision detection:
~~~~
def collisionChecker(S, initial_points, points, r, theta):
    for i in range(len(theta)):
        p_initial = np.array([
        [initial_points[0][i]],
        [initial_points[1][i]],
        [initial_points[2][i]],
        [1]
        ])

        temp = np.identity(4)

        for j in range(i+1):
            temp = np.dot(temp, expm(screwBracForm(S[j]) * theta[j]))

        p_final = np.dot(temp, p_initial)

        points[0].append(p_final[0][0])
        points[1].append(p_final[1][0])
        points[2].append(p_final[2][0])

    p = np.block([
    [np.reshape(np.asarray(points[0]),(1,len(theta)+2))],
    [np.reshape(np.asarray(points[1]),(1,len(theta)+2))],
    [np.reshape(np.asarray(points[2]),(1,len(theta)+2))]
    ])

    for i in range(len(theta) + 2):
        point1 = np.array([
        [p[0][i]],
        [p[1][i]],
        [p[2][i]]
        ])

        for j in range(len(theta) + 2 - i):
            if(i == i+j):
                continue

            point2 = np.array([
            [p[0][j+i]],
            [p[1][j+i]],
            [p[2][j+i]]
            ])

            if(norm(point1 - point2) <= r*2):
                return 1

    return 0
~~~~

