## Checkpoint 5 (due 4/11/18) [link to video](https://youtu.be/s9TSgrFPbqI):
### Demonstrate Motion/Path Planing  for UR3
In this checkpoint, the user would be to asked to input desired position of the UR3. 

First, make sure you have Remote API file and related Python files in the same folder as Checkpoint5.py.
You should also load the appropriate scene in the V-REP.

In the Scene, there are two concrete blocks which simulation recognizes as obstacles. In order to detect  
the obstacles, we implemented bounding sphere's inside the concrete blocks. The sphere in reality are simple  
dummy objects inside the simulation. The goal is to reach the desired pose as given by the user input.

### Sampling based planner Algorithm 
1). The path planing is implemented by sampling based on path planner algorithm. We set a tree  
structure to store all the intermediate paths to connect between the starting position and goal position.  
The tree structure has two trees: T(forward) and T(backward), which served as "bisection method".   

2). Then we sample random positions from random uniform distribution.

3). In order to generate the goal pose, we check whether the UR3 robot would encounter with   
a self-collision or a collision with obstacles.

4). If no collisions occur, add the current position/path to T(forward), and iteratively find new positions
in T(forward), while set the new positions as the child of previous position.

5). Do the same for T(backward).

6). Check if position belongs to both T(forward) and T(backward). If so, complete connected path from  
starting position to desired goal pose.


